package avalon

import "math/bits"

type Knowledge struct {
	n          int
	impossible [16]uint64
}

func (k *Knowledge) Init(players, reds int) {
	k.n = players
	max := 1 << uint(players)
	for i := 0; i < max; i++ {
		if bits.OnesCount16(uint16(i)) != reds {
			k.impossible[i>>6] |= 1 << uint(i&63)
		}
	}
	for i := max; i < len(k.impossible)*64; i++ {
		k.impossible[i>>6] |= 1 << uint(i&63)
	}
}

func (k *Knowledge) Remove(c uint16) {
	k.impossible[c>>6] |= 1 << (c & 63)
}

func (k *Knowledge) Possible(c uint16) bool {
	return k.impossible[c>>6]&(1<<(c&63)) == 0
}

func (k *Knowledge) MustBeBlue(player int) *Knowledge {
	if player < len(playerMasks) {
		mask := playerMasks[player]
		for i := range k.impossible {
			k.impossible[i] |= ^mask
		}
	} else {
		mask := playerMasks[player-len(playerMasks)]
		for i := range k.impossible {
			if mask&(1<<uint(i)) == 0 {
				k.impossible[i] = ^uint64(0)
			}
		}
	}
	return k
}

func (k *Knowledge) MustBeRed(player int) *Knowledge {
	if player < len(playerMasks) {
		mask := playerMasks[player]
		for i := range k.impossible {
			k.impossible[i] |= mask
		}
	} else {
		mask := playerMasks[player-len(playerMasks)]
		for i := range k.impossible {
			if mask&(1<<uint(i)) != 0 {
				k.impossible[i] = ^uint64(0)
			}
		}
	}
	return k
}

func (k *Knowledge) MustBe(player int, blue bool) *Knowledge {
	if blue {
		k.MustBeBlue(player)
	} else {
		k.MustBeRed(player)
	}
	return k
}

func (k *Knowledge) Not() *Knowledge {
	for i := range k.impossible {
		k.impossible[i] = ^k.impossible[i]
	}
	return k
}

// And removes all combinations from k that are impossible in x
func (k *Knowledge) And(x *Knowledge) *Knowledge {
	for i := range k.impossible {
		k.impossible[i] |= x.impossible[i]
	}
	return k
}

// Or adds all combinations to k that are possible in x
func (k *Knowledge) Or(x *Knowledge) *Knowledge {
	for i := range k.impossible {
		k.impossible[i] &= x.impossible[i]
	}
	return k
}

// Xor leaves k with the combinations that are possible in exactly one of x and k
func (k *Knowledge) Xor(x *Knowledge) *Knowledge {
	for i := range k.impossible {
		k.impossible[i] ^= x.impossible[i]
	}
	return k
}

// fast
func (k *Knowledge) Combinations() int {
	total := 0
	for _, m := range k.impossible {
		total += 64 - bits.OnesCount64(m)
	}
	return total
}

// Blue returns true if the player must be blue.
// fast
func (k *Knowledge) Blue(player int) bool {
	if player < len(playerMasks) {
		mask := playerMasks[player]
		for _, m := range k.impossible {
			if bits.OnesCount64(m|mask) < 64 {
				return false
			}
		}
	} else {
		mask := playerMasks[player-len(playerMasks)]
		for j, m := range k.impossible {
			if mask&(1<<uint(j)) == 0 && bits.OnesCount64(m) < 64 {
				return false
			}
		}
	}
	return true
}

// Red returns true if the player must be red.
// fast
func (k *Knowledge) Red(player int) bool {
	if player < len(playerMasks) {
		mask := playerMasks[player]
		for _, m := range k.impossible {
			if bits.OnesCount64(m|^mask) < 64 {
				return false
			}
		}
	} else {
		mask := playerMasks[player-len(playerMasks)]
		for j, m := range k.impossible {
			if mask&(1<<uint(j)) != 0 && bits.OnesCount64(m) < 64 {
				return false
			}
		}
	}
	return true
}

// CalculateRedness calculates, for each player, the number of possible combinations in which they are red.
// somewhat slow
func (k *Knowledge) CalculateRedness(r []int) {
	for i := range r {
		if i < len(playerMasks) {
			mask := playerMasks[i]
			for _, m := range k.impossible {
				r[i] += 64 - bits.OnesCount64(m|mask)
			}
		} else {
			mask := playerMasks[i-len(playerMasks)]
			for j, m := range k.impossible {
				if mask&(1<<uint(j)) == 0 {
					r[i] += 64 - bits.OnesCount64(m)
				}
			}
		}
	}
}

// CalculateMissionChance returns the number of possible combinations in which the mission will succeed.
// very slow if fails > 1
func (k *Knowledge) CalculateMissionChance(players []int, fails int) int {
	switch fails {
	case 0:
		return k.Combinations()
	case 1:
		var hmask, lmask uint64 = 0, 0
		for _, p := range players {
			if p < len(playerMasks) {
				lmask |= ^playerMasks[p]
			} else {
				hmask |= ^playerMasks[p-len(playerMasks)]
			}
		}
		blue := 0
		for i, m := range k.impossible {
			if hmask&(1<<uint(i)) == 0 {
				blue += 64 - bits.OnesCount64(m|lmask)
			}
		}
		return blue
	}

	var mask uint16
	for _, p := range players {
		mask |= 1 << uint(p)
	}

	blue := 0
	max := 1 << uint(k.n)
	for i := 0; i < max; i++ {
		if k.Possible(uint16(i)) {
			if bits.OnesCount16(uint16(i)&mask) < fails {
				blue++
			}
		}
	}
	return blue
}

// very slow
func (k *Knowledge) RemoveMissionResult(players []int, fails int) {
	var mask uint16
	for _, p := range players {
		mask |= 1 << uint(p)
	}

	max := 1 << uint(k.n)
	for i := 0; i < max; i++ {
		if k.Possible(uint16(i)) {
			if bits.OnesCount16(uint16(i)&mask) < fails {
				k.Remove(uint16(i))
			}
		}
	}
}

var playerMasks = [...]uint64{
	0x5555555555555555, // 01010101
	0x3333333333333333, // 00110011
	0x0F0F0F0F0F0F0F0F, // 00001111
	0x00FF00FF00FF00FF, //  etc.
	0x0000FFFF0000FFFF,
	0x00000000FFFFFFFF,
}
