package main

import (
	"math/rand"

	"bitbucket.org/johann_f/avalon"
)

func makeNames(n int, player string) []string {
	names := []string{"Martin", "Anton", "Raphael", "Stefanie", "Johann", "Isa", "Max", "Schirin", "Lukas", "Johannes", "Valentin", "Axel"}
	for i := range names {
		j := i + rand.Intn(len(names)-i)
		names[i], names[j] = names[j], names[i]
	}
	for i, name := range names {
		if name == player {
			names[i], names[0] = names[0], names[i]
			return names[:n]
		}
	}
	names[0] = player
	return names[:n]
}

var rules = &avalon.Rules{
	Characters:     []avalon.Character{avalon.Merlin, avalon.Parcival, avalon.Couple1, avalon.Couple1, avalon.Couple2, avalon.Couple2, avalon.Mordred, avalon.Morgana, avalon.Assassin, avalon.Oberon},
	MissionPlayers: []int{3, 4, 4, 5, 5},
	RequiredFails:  []int{1, 1, 1, 2, 1},
	Cards: avalon.DrawCardsFrom([]avalon.Card{
		avalon.SeePlayer, avalon.SeePlayer, avalon.SeePlayer,
		avalon.Veto, avalon.Veto, avalon.Veto,
		avalon.SeeMission, avalon.SeeMission,
		avalon.OpenMission, avalon.OpenMission,
	}, 2),
}
