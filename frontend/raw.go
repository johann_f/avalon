package main

import (
	"fmt"
	"net"
	"os"

	"bitbucket.org/johann_f/avalon"
)

func runRawServer(addr string) {
	l, err := net.Listen("tcp", addr)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return
	}
	for {
		conn, err := l.Accept()
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			return
		}
		go func() {
			names := makeNames(len(rules.Characters), "Remote Player")
			players := make([]*avalon.Connection, len(names))
			for i := range players {
				if i == 0 {
					players[i] = avalon.NewConnection(conn, conn)
				} else {
					cl, c := avalon.Pipe()
					players[i] = c
					go (&avalon.AIPlayer{}).Run(cl)
				}
			}
			avalon.RunGame(rules, names, players)
			for _, c := range players {
				c.Close()
			}
		}()
	}
}
