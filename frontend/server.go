package main

import (
	"bytes"
	"fmt"
	"html/template"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"sync"
	"time"

	"bitbucket.org/johann_f/avalon"
)

const sessionTimeout = 30 * time.Minute

func main() {
	const raw = true
	const tls = false

	rand.Seed(time.Now().Unix())
	if raw {
		go runRawServer(":5005")
	}
	var s Server
	s.sessions = make(map[string]*Session)
	if !tls {
		go http.ListenAndServe(":8080", &s)
		fmt.Print("Server running, press enter to stop")
		os.Stdin.Read([]byte{0})
	}
	if tls {
		go http.ListenAndServe(":80", http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
			http.Redirect(rw, req, "https://"+req.Host+req.URL.Path, http.StatusMovedPermanently)
		}))
		http.ListenAndServeTLS(":443", "/etc/letsencrypt/live/jfreymuth.de/fullchain.pem", "/etc/letsencrypt/live/jfreymuth.de/privkey.pem", http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
			switch req.URL.Path {
			case "/avalon":
				s.ServeHTTP(rw, req)
			case "/reset":
				s.Lock()
				s.sessions = make(map[string]*Session)
				s.Unlock()
			}
		}))
	}
}

type Server struct {
	sessions map[string]*Session
	sync.Mutex
}

func (s *Server) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	sid := req.PostFormValue("session")
	s.Lock()
	session, ok := s.sessions[sid]
	s.Unlock()
	if !ok {
		session = s.NewSession()
	}
	select {
	case session.Requests <- req:
	case <-session.Timeout:
		http.Error(rw, "session timeout", http.StatusInternalServerError)
		return
	}
	select {
	case page := <-session.Pages:
		err := page.Template.Execute(rw, page.Data)
		if err != nil {
			fmt.Println(err)
		}
	case <-session.Timeout:
		http.Error(rw, "session timeout", http.StatusInternalServerError)
	}
}

func (s *Server) NewSession() *Session {
	session := &Session{
		Pages:    make(chan Page),
		Requests: make(chan *http.Request),
		Timeout:  make(chan struct{}),
	}
	s.Lock()
	for {
		session.ID = strconv.FormatUint(rand.Uint64(), 16)
		if _, ok := s.sessions[session.ID]; !ok {
			break
		}
	}
	s.sessions[session.ID] = session
	s.Unlock()
	go func() {
		session.Run()
		s.Lock()
		delete(s.sessions, session.ID)
		s.Unlock()
	}()
	go func() {
		time.Sleep(sessionTimeout)
		close(session.Timeout)
	}()
	return session
}

type Session struct {
	Pages    chan Page
	Requests chan *http.Request
	Timeout  chan struct{}

	ID string

	TemplateBase TemplateBase
}

type Page struct {
	Template *template.Template
	Data     interface{}
}

func (s *Session) Run() {
	s.TemplateBase.Session = s.ID
	<-s.Requests
	req := s.Show(Page{templateEnterName, &s.TemplateBase})
	if req == nil {
		return
	}
	name := req.PostFormValue("name")
	names := makeNames(len(rules.Characters), name)
	ai := make([]*avalon.Connection, len(rules.Characters))
	for i := range ai {
		cl, c := avalon.Pipe()
		ai[i] = c
		if i == 0 {
			go s.Play(cl)
		} else {
			go (&avalon.AIPlayer{}).Run(cl)
		}
	}
	result := avalon.RunGame(rules, names, ai)
	for _, c := range ai {
		c.Close()
	}
	fmt.Println(name, result)
}

func (s *Session) Show(p Page) *http.Request {
	select {
	case s.Pages <- p:
	case <-s.Timeout:
		return nil
	}
	select {
	case r := <-s.Requests:
		return r
	case <-s.Timeout:
		return nil
	}
}

func (s *Session) Log(args ...interface{}) {
	s.TemplateBase.Log = append(s.TemplateBase.Log, fmt.Sprint(args...))
}

func (s *Session) LogNext() {
	s.TemplateBase.OldLogs = append(s.TemplateBase.OldLogs, s.TemplateBase.Log)
	s.TemplateBase.Log = nil
}

func (s *Session) Play(c *avalon.Client) {
	for {
		msg, ok := c.Next()
		if !ok {
			break
		}
		switch msg {
		case "RulesCharacters":
			s.TemplateBase.Rules.Characters = c.Characters()
		case "RulesMissionPlayers":
			s.TemplateBase.Rules.MissionPlayers = c.Ints()
		case "RulesMissionFails":
			s.TemplateBase.Rules.RequiredFails = c.Ints()
		case "SetupNames":
			s.TemplateBase.Names = c.Strings()
		case "SetupIndex":
			s.TemplateBase.Index = c.Int()
		case "SetupCharacter":
			s.TemplateBase.Character = c.Character()
		case "SetupHints":
			s.TemplateBase.Hints = c.Ints()
			s.Show(Page{templateSetup, &s.TemplateBase})
		case "TurnStart":
			s.TemplateBase.Mission = c.Int()
			s.TemplateBase.Attempt = c.Int()
			s.TemplateBase.Leader = c.Int()
			s.LogNext()
			s.Log("Mission ", s.TemplateBase.Mission+1, ", ", s.TemplateBase.Attempt, " Abstimmungen fehlgeschlagen, Startspieler ist ", s.TemplateBase.Names[s.TemplateBase.Leader])
		case "DistributeCards":
			cards := c.Cards()
			if req := s.Show(Page{templateDistribute, TemplateDistribute{&s.TemplateBase, cards}}); req != nil {
				reply := ""
				for i := range cards {
					if i != 0 {
						reply += ", "
					}
					reply += req.PostFormValue(fmt.Sprint("card", i))
				}
				c.Reply(reply)
			} else {
				c.Reply("")
			}
		case "CardGiven":
			card := c.Card()
			player := c.Int()
			if player == s.TemplateBase.Index {
				s.TemplateBase.Cards = append(s.TemplateBase.Cards, card)
			}
			s.Log(s.TemplateBase.Names[s.TemplateBase.Leader], " hat ", translate(card), " an ", s.TemplateBase.Names[player], " gegeben")
		case "UseCard":
			//card := c.Card()
			if req := s.Show(Page{templateUseCard, &s.TemplateBase}); req != nil {
				reply := req.PostFormValue("player")
				s.TemplateBase.Seen, _ = strconv.Atoi(reply)
				c.Reply(reply)
			} else {
				c.Reply("")
			}
		case "SeePlayer":
			blue := c.String() == "blue"
			if req := s.Show(Page{templateSeePlayer, TemplateSeePlayer{&s.TemplateBase, blue}}); req != nil {
				if req.PostFormValue("blue") != "" {
					c.Reply("blue")
				} else {
					c.Reply("red")
				}
			} else {
				c.Reply("")
			}
		case "SeePlayerClaim":
			from := c.Int()
			about := c.Int()
			blue := c.String() == "blue"
			if blue {
				s.Log(s.TemplateBase.Names[from], ": \"", s.TemplateBase.Names[about], " ist blau\"")
			} else {
				s.Log(s.TemplateBase.Names[from], ": \"", s.TemplateBase.Names[about], " ist rot\"")
			}
		case "ProposeMission":
			players := make([]int, s.TemplateBase.Rules.MissionPlayers[s.TemplateBase.Mission])
			for i := range players {
				players[i] = i
			}
			if req := s.Show(Page{templateProposeMission, TemplateProposeMission{&s.TemplateBase, players}}); req != nil {
				reply := ""
				for i := range players {
					if i != 0 {
						reply += ", "
					}
					reply += req.PostFormValue(fmt.Sprint("player", i))
				}
				c.Reply(reply)
			} else {
				c.Reply("")
			}
		case "VoteOnMission":
			s.TemplateBase.MissionPlayers = c.Ints()
			s.Log("Vorschlag: ", names(s.TemplateBase.Names, s.TemplateBase.MissionPlayers))
			if req := s.Show(Page{templateVote, &s.TemplateBase}); req != nil {
				if req.PostFormValue("yes") != "" {
					c.Reply("yes")
				} else {
					c.Reply("no")
				}
			} else {
				c.Reply("")
			}
		case "VoteResult":
			s.TemplateBase.Vote = c.Votes()
			no := 0
			for _, v := range s.TemplateBase.Vote {
				if !v {
					no++
				}
			}
			if no*2 >= len(s.TemplateBase.Names) {
				s.Log("Mission abgelehnt")
			}
			s.Show(Page{templateVoteResult, &s.TemplateBase})
		case "UseVetoCard":
			if req := s.Show(Page{templateVeto, &s.TemplateBase}); req != nil {
				if req.PostFormValue("veto") != "" {
					c.Reply("veto")
				} else {
					c.Reply("")
				}
			} else {
				c.Reply("")
			}
		case "MissionVetoed":
			vetoer := c.Int()
			s.Log(s.TemplateBase.Names[vetoer], " hat Misstrauen eingesetzt")
			s.Show(Page{templateVetoResult, TemplateVetoResult{&s.TemplateBase, vetoer}})
		case "UseOpenCard":
			if req := s.Show(Page{templateUseOpen, &s.TemplateBase}); req != nil {
				if req.PostFormValue("use") != "" {
					c.Reply(req.PostFormValue("player"))
				} else {
					c.Reply("")
				}
			} else {
				c.Reply("")
			}
		case "GoOnMission":
			if req := s.Show(Page{templateMission, TemplateMission{&s.TemplateBase, false}}); req != nil {
				if req.PostFormValue("blue") != "" {
					c.Reply("blue")
				} else {
					c.Reply("red")
				}
			} else {
				c.Reply("")
			}
		case "GoOnMissionOpen":
			if req := s.Show(Page{templateMission, TemplateMission{&s.TemplateBase, true}}); req != nil {
				if req.PostFormValue("blue") != "" {
					c.Reply("blue")
				} else {
					c.Reply("red")
				}
			} else {
				c.Reply("")
			}
		case "OpenMissionResult":
			player := c.Int()
			blue := c.String() == "blue"
			if blue {
				s.Log(s.TemplateBase.Names[player], " spielt Erforlg (offen)")
			} else {
				s.Log(s.TemplateBase.Names[player], " spielt Fehlschlag (offen)")
			}
		case "UseSeeMissionCard":
			if req := s.Show(Page{templateUseSeeMission, &s.TemplateBase}); req != nil {
				if req.PostFormValue("use") != "" {
					reply := req.PostFormValue("player")
					s.TemplateBase.Seen, _ = strconv.Atoi(reply)
					c.Reply(reply)
				} else {
					c.Reply("")
				}
			} else {
				c.Reply("")
			}
		case "SeeMission":
			blue := c.String() == "blue"
			if req := s.Show(Page{templateSeeMission, TemplateSeePlayer{&s.TemplateBase, blue}}); req != nil {
				if req.PostFormValue("blue") != "" {
					c.Reply("blue")
				} else {
					c.Reply("red")
				}
			} else {
				c.Reply("")
			}
		case "SeeMissionClaim":
			from := c.Int()
			about := c.Int()
			blue := c.String() == "blue"
			if blue {
				s.Log(s.TemplateBase.Names[from], ": \"", s.TemplateBase.Names[about], " hat Erforlg gespielt\"")
			} else {
				s.Log(s.TemplateBase.Names[from], ": \"", s.TemplateBase.Names[about], " hat Fehlschlag gespielt\"")
			}
		case "MissionResult":
			fails := c.Int()
			blue := c.String() == "blue"
			plural := " Fehlschläge)"
			if fails == 1 {
				plural = " Fehlschlag)"
			}
			if blue {
				s.Log("Mission war erfolgreich (", fails, plural)
			} else {
				s.Log("Mission ist fehlgeschlagen (", fails, plural)
			}
			s.Show(Page{templateVoteResult, &s.TemplateBase})
		case "GameOver":
			s.LogNext()
			blue := c.String() == "blue"
			if blue {
				s.Log("Blau gewinnt")
			} else {
				s.Log("Rot gewinnt")
			}
		case "RevealPlayers":
			s.LogNext()
			chars := c.Characters()
			for i := range chars {
				s.Log(s.TemplateBase.Names[i], " war ", translate(chars[i]))
			}
			s.LogNext()
			select {
			case s.Pages <- Page{templateSetup, &s.TemplateBase}:
			case <-s.Timeout:
			}
		case "GameAborted":
			s.LogNext()
			s.Log("Spiel abgebrochen")
			s.LogNext()
			select {
			case s.Pages <- Page{templateSetup, &s.TemplateBase}:
			case <-s.Timeout:
			}
		case "Error":
			fmt.Println(c.String())
		}
	}
}

type TemplateBase struct {
	Session string

	Rules     avalon.Rules
	Names     []string
	Index     int
	Character avalon.Character
	Hints     []int

	Cards          []avalon.Card
	Mission        int
	Attempt        int
	Leader         int
	Seen           int
	MissionPlayers []int
	Vote           []bool

	Log     []string
	OldLogs [][]string
}

type TemplateDistribute struct {
	*TemplateBase
	C []avalon.Card
}
type TemplateSeePlayer struct {
	*TemplateBase
	SeenBlue bool
}
type TemplateProposeMission struct {
	*TemplateBase
	I []int
}
type TemplateVetoResult struct {
	*TemplateBase
	Vetoer int
}
type TemplateMission struct {
	*TemplateBase
	Open bool
}

var templateEnterName = template.Must(template.New("enterName").Funcs(templateFuncs).Parse(header + `
<div class="w3-panel w3-light-gray w3-border w3-round"><form method="post">
	<label>Name:</label>
	<input class="w3-input w3-border" type="text" name="name" />
	<input class="w3-btn w3-white w3-margin" type="submit" value="Weiter" />
	<input type="hidden" name="session" value="{{.Session}}" />
</form></div>
` + footer))

var templateSetup = template.Must(template.New("setup").Funcs(templateFuncs).Parse(header + `
` + status + `
<div class="w3-panel w3-light-gray w3-border w3-round"><form method="post">
	<input class="w3-btn w3-white w3-margin" type="submit" value="Weiter" />
	<input type="hidden" name="session" value="{{.Session}}" />
</form></div>
` + footer))

var templateDistribute = template.Must(template.New("distribute").Funcs(templateFuncs).Parse(header + `
<div class="w3-panel w3-light-gray w3-border w3-round"><form method="post">
	` + log + `
	<p>Verteile Karten</p>
	{{- $names := .Names}}
	{{- range $i, $card := .C}}
	<label>{{translate $card}}</label><select class="w3-select" name="card{{$i}}">{{range $i, $name := $names}}
		<option value="{{$i}}">{{$name}}</option>{{end}}
	</select>{{end}}
	<input class="w3-btn w3-white w3-margin" type="submit" value="Weiter" />
	<input type="hidden" name="session" value="{{.Session}}" />
</form></div>
` + status + `
` + footer))

var templateUseCard = template.Must(template.New("useCard").Funcs(templateFuncs).Parse(header + `
<div class="w3-panel w3-light-gray w3-border w3-round"><form method="post">
	` + log + `
	<label>Wessen Farbe möchtest du sehen?</label>
	<select class="w3-select" name="player">{{range $i, $name := .Names}}
		<option value="{{$i}}">{{$name}}</option>{{end}}
	</select>
	<input class="w3-btn w3-white w3-margin" type="submit" value="Weiter" />
	<input type="hidden" name="session" value="{{.Session}}" />
</form></div>
` + status + `
` + footer))

var templateSeePlayer = template.Must(template.New("seePlayer").Funcs(templateFuncs).Parse(header + `
<div class="w3-panel w3-light-gray w3-border w3-round"><form method="post">
	` + log + `
	<p>{{index .Names .Seen}} ist {{if .SeenBlue}}blau{{else}}rot{{end}}</p>
	<input class="w3-btn w3-white w3-margin" type="submit" name="blue" value="Blau" />
	<input class="w3-btn w3-white w3-margin" type="submit" name="red" value="Rot" />
	<input type="hidden" name="session" value="{{.Session}}" />
</form></div>
` + status + `
` + footer))

var templateProposeMission = template.Must(template.New("proposeMission").Funcs(templateFuncs).Parse(header + `
<div class="w3-panel w3-light-gray w3-border w3-round"><form method="post">
	` + log + `
	<label>Schlage eine Mission vor</label>
	{{$names := .Names}}
	{{- range .I}}
	<select class="w3-select" name="player{{.}}">{{range $i, $name := $names}}
		<option value="{{$i}}">{{$name}}</option>{{end}}
	</select>{{end}}
	<input class="w3-btn w3-white w3-margin" type="submit" value="Weiter" />
	<input type="hidden" name="session" value="{{.Session}}" />
</form></div>
` + status + `
` + footer))

var templateVote = template.Must(template.New("vote").Funcs(templateFuncs).Parse(header + `
<div class="w3-panel w3-light-gray w3-border w3-round"><form method="post">
	` + log + `
	<input class="w3-btn w3-white w3-margin" type="submit" name="yes" value="Ja" />
	<input class="w3-btn w3-white w3-margin" type="submit" name="no" value="Nein" />
	<input type="hidden" name="session" value="{{.Session}}" />
</form></div>
` + status + `
` + footer))

var templateVoteResult = template.Must(template.New("voteResult").Funcs(templateFuncs).Parse(header + `
<div class="w3-panel w3-light-gray w3-border w3-round"><form method="post">
	` + log + `
	` + vote + `
	<input class="w3-btn w3-white w3-margin" type="submit" value="Weiter" />
	<input type="hidden" name="session" value="{{.Session}}" />
</form></div>
` + status + `
` + footer))

var templateVeto = template.Must(template.New("veto").Funcs(templateFuncs).Parse(header + `
<div class="w3-panel w3-light-gray w3-border w3-round"><form method="post">
	` + log + `
	` + vote + `
	<input class="w3-btn w3-white w3-margin" type="submit" name="veto" value="Misstrauen" />
	<input class="w3-btn w3-white w3-margin" type="submit" value="Weiter" />
	<input type="hidden" name="session" value="{{.Session}}" />
</form></div>
` + status + `
` + footer))

var templateVetoResult = template.Must(template.New("vetoResult").Funcs(templateFuncs).Parse(header + `
<div class="w3-panel w3-light-gray w3-border w3-round"><form method="post">
	` + log + `
	` + vote + `
	<p>{{index .Names .Vetoer}} hat Misstrauen eingesetzt.</p>
	<input class="w3-btn w3-white w3-margin" type="submit" value="Weiter" />
	<input type="hidden" name="session" value="{{.Session}}" />
</form></div>
` + status + `
` + footer))

var templateUseOpen = template.Must(template.New("useOpen").Funcs(templateFuncs).Parse(header + `
<div class="w3-panel w3-light-gray w3-border w3-round"><form method="post">
	` + log + `
	` + vote + `
	<p>Die Abstimmung war erfolgreich.</p>
	<p>Du kannst jemanden offen spielen lassen</p>
	{{- $names := .Names}}
	<select class="w3-select" name="player">{{range .MissionPlayers}}
		<option value="{{.}}">{{index $names .}}</option>{{end}}
	</select>
	<input class="w3-btn w3-white w3-margin" type="submit" name="use" value="Im Fokus" />
	<input class="w3-btn w3-white w3-margin" type="submit" value="Nicht verwenden" />
	<input type="hidden" name="session" value="{{.Session}}" />
</form></div>
` + status + `
` + footer))

var templateMission = template.Must(template.New("mission").Funcs(templateFuncs).Parse(header + `
<div class="w3-panel w3-light-gray w3-border w3-round"><form method="post">
	` + log + `
	<p>Die Abstimmung war erfolgreich.</p>{{if .Open}}
	<p>Du must offen spielen</p>{{end}}
	<input class="w3-btn w3-white w3-margin" type="submit" name="blue" value="Erfolg" />
	<input class="w3-btn w3-white w3-margin" type="submit" name="red" value="Fehlschlag" />
	<input type="hidden" name="session" value="{{.Session}}" />
</form></div>
` + status + `
` + footer))

var templateUseSeeMission = template.Must(template.New("useSeeMission").Funcs(templateFuncs).Parse(header + `
<div class="w3-panel w3-light-gray w3-border w3-round"><form method="post">
	` + log + `
	` + vote + `
	<p>Die Abstimmung war erfolgreich.</p>
	<p>Du kannst eine Überwachung einsetzen</p>
	{{- $names := .Names}}
	<select class="w3-select" name="player">{{range .MissionPlayers}}
		<option value="{{.}}">{{index $names .}}</option>{{end}}
	</select>
	<input class="w3-btn w3-white w3-margin" type="submit" name="use" value="Überwachung" />
	<input class="w3-btn w3-white w3-margin" type="submit" value="Nicht verwenden" />
	<input type="hidden" name="session" value="{{.Session}}" />
</form></div>
` + status + `
` + footer))

var templateSeeMission = template.Must(template.New("seeMission").Funcs(templateFuncs).Parse(header + `
<div class="w3-panel w3-light-gray w3-border w3-round"><form method="post">
	` + log + `
	<p>{{index .Names .Seen}} hat {{if .SeenBlue}}Erfolg{{else}}Fehlschlag{{end}} gespielt</p>
	<input class="w3-btn w3-white w3-margin" type="submit" name="blue" value="Blau" />
	<input class="w3-btn w3-white w3-margin" type="submit" name="red" value="Rot" />
	<input type="hidden" name="session" value="{{.Session}}" />
</form></div>
` + status + `
` + footer))

const header = `<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Avalon</title>
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body class="w3-content">`

const log = `<p>{{range .Log}}
		{{.}}<br/>{{end}}
	</p>`

const status = `<div class="w3-container w3-section w3-light-gray w3-border w3-round">
	<p>Du bist {{translate .Character}}</p>
	<p>{{hints .Names .Character .Hints}}</p>
	{{- range .OldLogs}}
	<p>{{range .}}
		{{.}}<br/>{{end}}
	</p>{{end}}
</div>`

const mission = `<p>{{index .Names .Leader}} hat folgende Mission vorgeschlagen: {{names .Names .MissionPlayers}}</p>`
const vote = `<p>Abstimmung:<br/>{{$names := .Names}}{{range $i, $vote := .Vote}}
{{- index $names $i}}: {{if $vote}}Ja{{else}}Nein{{end}}<br/>{{end -}}
</p>`

const footer = `</body>
</html>
`

func translate(i interface{}) string {
	switch i {
	case avalon.Merlin:
		return "Merlin"
	case avalon.Parcival:
		return "Pacival"
	case avalon.Couple1:
		return "Blau ohne Helm"
	case avalon.Couple2:
		return "Blau mit Helm"
	case avalon.Blue:
		return "Blau"
	case avalon.Mordred:
		return "Mordred"
	case avalon.Morgana:
		return "Morgana"
	case avalon.Assassin:
		return "Meuchelmörder"
	case avalon.Oberon:
		return "Oberon"
	case avalon.Red:
		return "Rot"
	case avalon.Veto:
		return "Misstrauen"
	case avalon.SeePlayer:
		return "Abhörmaßnahme"
	case avalon.SeeMission:
		return "Überwachung"
	case avalon.OpenMission:
		return "Im Fokus"
	}
	return "<?>"
}

func names(names []string, x []int) string {
	var buf bytes.Buffer
	for i, j := range x {
		if len(x) > 1 && i == len(x)-1 {
			buf.WriteString(" und ")
		} else if i != 0 {
			buf.WriteString(", ")
		}
		buf.WriteString(names[j])
	}
	return buf.String()
}

var templateFuncs = template.FuncMap{"translate": translate, "names": names, "hints": func(n []string, c avalon.Character, hints []int) string {
	var buf bytes.Buffer
	switch c {
	case avalon.Merlin, avalon.Mordred, avalon.Morgana, avalon.Assassin, avalon.Red:
		buf.WriteString("Rote Spieler sind ")
		buf.WriteString(names(n, hints))
	case avalon.Parcival:
		buf.WriteString("Potentielle Merlins sind ")
		buf.WriteString(names(n, hints))
	case avalon.Couple1, avalon.Couple2:
		buf.WriteString("Blaue Spieler sind ")
		buf.WriteString(names(n, hints))
	}
	return buf.String()
}}
