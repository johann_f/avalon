package avalon

import (
	"fmt"
	"math/rand"
)

type Character int

const (
	Merlin Character = iota
	Parcival
	Couple1
	Couple2
	Blue

	Mordred
	Morgana
	Assassin
	Oberon
	Red
)

func (c Character) Blue() bool {
	return c <= Blue
}

func (c Character) String() string {
	return [...]string{"Merlin", "Parcival", "Couple1", "Couple2", "Blue", "Mordred", "Morgana", "Assassin", "Oberon", "Red"}[c]
}

func (c *Character) Parse(s string) bool {
	var ok bool
	*c, ok = map[string]Character{"Merlin": Merlin, "Parcival": Parcival, "Couple1": Couple1, "Couple2": Couple2, "Blue": Blue, "Mordred": Mordred, "Morgana": Morgana, "Assassin": Assassin, "Oberon": Oberon, "Red": Red}[s]
	return ok
}

type Card int

const (
	Veto Card = iota
	Leadership
	SeePlayer
	SeeNeighbor
	SeeLeader
	SeeMission
	OpenMission
)

func (c Card) String() string {
	return [...]string{"Veto", "Leadership", "SeePlayer", "SeeNeighbor", "SeeLeader", "SeeMission", "OpenMission"}[c]
}

func (c *Card) Parse(s string) bool {
	var ok bool
	*c, ok = map[string]Card{"Veto": Veto, "Leadership": Leadership, "SeePlayer": SeePlayer, "SeeNeighbor": SeeNeighbor, "SeeLeader": SeeLeader, "SeeMission": SeeMission, "OpenMission": OpenMission}[s]
	return ok
}

type Rules struct {
	Characters     []Character
	MissionPlayers []int
	RequiredFails  []int
	Cards          func(int) []Card
}

func DrawCardsFrom(pool []Card, perTurn int) func(int) []Card {
	var n int
	p := append([]Card{}, pool...)

	return func(mission int) []Card {
		if mission == 0 {
			n = len(pool)
		}
		if perTurn > n {
			perTurn = n
		}
		cards := make([]Card, perTurn)
		for i := range cards {
			j := rand.Intn(n)
			cards[i] = p[j]
			n--
			p[j], p[n] = p[n], p[j]
		}
		return cards
	}
}

func blueRed(blue bool) string {
	if blue {
		return "blue"
	}
	return "red"
}

func yesNo(yes bool) string {
	if yes {
		return "yes"
	}
	return "no"
}

type Result int

const (
	BlueVictory Result = iota
	RedVictory
	Error
)

func (r Result) String() string {
	switch r {
	case BlueVictory:
		return "BlueVictory"
	case RedVictory:
		return "RedVictory"
	case Error:
		return "Error"
	}
	return "<invalid>"
}

func RunGame(rules *Rules, names []string, ai []*Connection) (gameResult Result) {
	if names == nil {
		names = make([]string, len(ai))
		for i := range names {
			names[i] = fmt.Sprint("player", i)
		}
	}

	// send rules
	for _, ai := range ai {
		ai.Write("RulesCharacters", rules.Characters)
		ai.Write("RulesMissionPlayers", rules.MissionPlayers)
		ai.Write("RulesMissionFails", rules.RequiredFails)
	}

	// shuffle characters
	chars := append([]Character(nil), rules.Characters...)
	for i := range chars {
		j := i + rand.Intn(len(chars)-i)
		chars[i], chars[j] = chars[j], chars[i]
	}

	// create hints
	var redHints, merlinHints, parcivalHints, couple1Hints, couple2Hints []int
	for i, char := range chars {
		if !char.Blue() {
			if char != Oberon {
				redHints = append(redHints, i)
			}
			if char != Mordred {
				merlinHints = append(merlinHints, i)
			}
		}
		switch char {
		case Merlin, Morgana:
			parcivalHints = append(parcivalHints, i)
		case Couple1:
			couple1Hints = append(couple1Hints, i)
		case Couple2:
			couple2Hints = append(couple2Hints, i)
		}
	}

	// send setup messages
	for i, ai := range ai {
		var hints []int
		switch chars[i] {
		case Merlin:
			hints = merlinHints
		case Parcival:
			hints = parcivalHints
		case Couple1:
			hints = couple1Hints
		case Couple2:
			hints = couple2Hints
		default:
			if !chars[i].Blue() && chars[i] != Oberon {
				hints = redHints
			}
		}
		ai.Write("SetupNames", names)
		ai.Write("SetupIndex", i)
		ai.Write("SetupCharacter", chars[i])
		ai.Write("SetupHints", hints)
	}

	var (
		mission      = 0
		blueMissions = 0
		redMissions  = 0
		leader       = rand.Intn(len(chars))

		cards = make([][]Card, len(chars))
	)
	for {
		// start turn
		for _, ai := range ai {
			ai.Write("TurnStart", mission, 0, leader)
		}

		// distribute cards
		cardsDrawn := rules.Cards(mission)
		if len(cardsDrawn) > 0 {
			ai[leader].Write("DistributeCards", cardsDrawn)
			players := ai[leader].ReadInts()
			if !check(ai[leader], ai) {
				return Error
			}
			if len(players) != len(cardsDrawn) {
				err(ai[leader], ai, "protocol error (wrong number of players)")
				return Error
			}
			for i, p := range players {
				if p == leader {
					err(ai[leader], ai, "illegal move (leader can not give cards to themselves)")
					return Error
				}
				for j, q := range players {
					if i != j && p == q {
						err(ai[leader], ai, "illegal move (leader can not give multiple cards to the same player)")
						return Error
					}
				}
			}
			for i, card := range cardsDrawn {
				for _, ai := range ai {
					ai.Write("CardGiven", card, players[i])
				}
				cards[players[i]] = append(cards[players[i]], card)
			}
		}

		// use cards
		for i := range cards {
		repeat:
			for j, c := range cards[i] {
				switch c {
				case SeePlayer, SeeNeighbor, SeeLeader:
					ai[i].Write("UseCard", c)
					player := ai[i].ReadInt()
					if !check(ai[i], ai) {
						return Error
					}
					ai[i].Write("SeePlayer", blueRed(chars[player].Blue()))
					claim := ai[i].ReadBool("blue", "red")
					if !check(ai[i], ai) {
						return Error
					}
					for _, ai := range ai {
						ai.Write("SeePlayerClaim", i, player, blueRed(claim))
					}
					cards[i] = append(cards[i][:j], cards[i][j+1:]...)
					goto repeat
				}
			}
		}

		attempts := 0
		for {
			if attempts == 5 {
				gameResult = RedVictory
				goto gameOver
			}
			if attempts != 0 {
				leader++
				if leader == len(chars) {
					leader = 0
				}
				for _, ai := range ai {
					ai.Write("TurnStart", mission, attempts, leader)
				}
			}

			// propose mission
			ai[leader].Write("ProposeMission")
			players := ai[leader].ReadInts()
			if !check(ai[leader], ai) {
				return Error
			}
			if len(players) != rules.MissionPlayers[mission] {
				err(ai[leader], ai, "protocol error (wrong number of players)")
				return Error
			}
			for i, p := range players {
				for j, q := range players {
					if i != j && p == q {
						err(ai[leader], ai, "illegal move (all players on a mission must be different)")
						return Error
					}
				}
			}

			// vote on mission
			vote := make([]bool, len(chars))
			for i := range ai {
				ai[i].Write("VoteOnMission", players)
				vote[i] = ai[i].ReadBool("yes", "no")
				if !check(ai[i], ai) {
					return Error
				}
			}
			voteResult := 0
			for _, v := range vote {
				if v {
					voteResult++
				}
			}
			voteAcc := voteResult > len(chars)/2
			for _, ai := range ai {
				ai.Write("VoteResult", vote)
			}
			if !voteAcc {
				attempts++
				continue
			}

			// use veto cards
			veto := false
			vetoer := 0
		veto:
			for i := range cards {
				for j, c := range cards[i] {
					if c == Veto {
						ai[i].Write("UseVetoCard")
						reply := ai[i].ReadBool("veto", "")
						if !check(ai[i], ai) {
							return Error
						}
						if reply {
							cards[i] = append(cards[i][:j], cards[i][j+1:]...)
							veto = true
							vetoer = i
							break veto
						}
						break
					}
				}
			}
			if veto {
				for _, ai := range ai {
					ai.Write("MissionVetoed", vetoer)
				}
				attempts++
				continue
			}

			// use open cards
			open := make([]bool, len(chars))
			for i := range cards {
			repeatOpenCard:
				for j, c := range cards[i] {
					if c == OpenMission {
						ai[i].Write("UseOpenCard")
						player, ok := ai[i].ReadOptionalInt()
						if !check(ai[i], ai) {
							return Error
						}
						if ok {
							open[player] = true
							cards[i] = append(cards[i][:j], cards[i][j+1:]...)
							goto repeatOpenCard
						} else {
							break
						}
					}
				}
			}

			// do mission
			missionResult := make([]bool, len(players))
			for i, p := range players {
				if open[p] {
					ai[p].Write("GoOnMissionOpen")
				} else {
					ai[p].Write("GoOnMission")
				}
				missionResult[i] = ai[p].ReadBool("blue", "red")
				if !check(ai[i], ai) {
					return Error
				}
			}
			for i, p := range players {
				if open[p] {
					for _, ai := range ai {
						ai.Write("OpenMissionResult", p, blueRed(missionResult[i]))
					}
				}
			}
			fails := 0
			for _, m := range missionResult {
				if !m {
					fails++
				}
			}
			result := fails < rules.RequiredFails[mission]

			// use see mission cards
			for i := range cards {
			repeatSeeMissionCard:
				for j, c := range cards[i] {
					if c == SeeMission {
						ai[i].Write("UseSeeMissionCard")
						player, ok := ai[i].ReadOptionalInt()
						if !check(ai[i], ai) {
							return Error
						}
						if ok {
							var claim bool
							for k, p := range players {
								if p == player {
									ai[i].Write("SeeMission", blueRed(missionResult[k]))
									claim = ai[i].ReadBool("blue", "red")
									if !check(ai[i], ai) {
										return Error
									}
									break
								}
							}
							for _, ai := range ai {
								ai.Write("SeeMissionClaim", i, player, blueRed(claim))
							}
							cards[i] = append(cards[i][:j], cards[i][j+1:]...)
							goto repeatSeeMissionCard
						} else {
							break
						}
					}
				}
			}

			// send mission results
			for _, ai := range ai {
				ai.Write("MissionResult", fails, blueRed(result))
			}

			// update score
			if result {
				blueMissions++
				if blueMissions == 3 {
					gameResult = BlueVictory
					goto gameOver
				}
			} else {
				redMissions++
				if redMissions == 3 {
					gameResult = RedVictory
					goto gameOver
				}
			}
			break
		}
		leader++
		if leader == len(chars) {
			leader = 0
		}
		mission++
	}
gameOver:
	switch gameResult {
	case BlueVictory:
		for _, ai := range ai {
			ai.Write("GameOver", "blue")
		}
	case RedVictory:
		for _, ai := range ai {
			ai.Write("GameOver", "red")
		}
	}
	for _, ai := range ai {
		ai.Write("RevealPlayers", chars)
	}
	return
}

func err(r *Connection, ai []*Connection, msg string) {
	r.Write("Error", msg)
	for _, ai := range ai {
		ai.Write("GameAborted")
	}
}

func check(r *Connection, ai []*Connection) bool {
	if r.err != "" {
		err(r, ai, fmt.Sprint("protocol error (", r.err, ")"))
		return false
	}
	return true
}
