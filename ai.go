package avalon

import (
	"fmt"
	"math/rand"
	"os"
	"sort"
	"strconv"
)

type AIPlayer struct {
	index     int
	character Character
	names     []string
	rules     Rules

	knowledge Knowledge
	public    Knowledge

	mission        int
	attempt        int
	leader         int
	missionPlayers []int
	missionVote    []bool
	open           []openMission
	claims         []missionClaim
	seenPlayer     int
	seenMission    bool
	vetoCards      int

	blueVictories int
	redVictories  int
}

type openMission struct {
	player int
	blue   bool
}

type missionClaim struct {
	from, about int
	blue        bool
}

func (p *AIPlayer) Run(c *Client) {
	for {
		msg, ok := c.Next()
		if !ok {
			return
		}
		switch msg {
		case "RulesCharacters":
			p.rules.Characters = c.Characters()
		case "RulesMissionPlayers":
			p.rules.MissionPlayers = c.Ints()
		case "RulesMissionFails":
			p.rules.RequiredFails = c.Ints()
		case "SetupNames":
			p.names = c.Strings()
		case "SetupIndex":
			p.index = c.Int()
		case "SetupCharacter":
			p.character = c.Character()
		case "SetupHints":
			p.setupKnowledge(c.Ints())
		case "TurnStart":
			p.mission = c.Int()
			p.attempt = c.Int()
			p.leader = c.Int()
			p.open = nil
		case "DistributeCards":
			c.ReplyInts(p.distributeCards(c.Cards()))
		case "CardGiven":
			card := c.Card()
			c.Int()
			if card == Veto {
				p.vetoCards++
			}
			// TODO
		case "UseCard":
			card := c.Card()
			p.seenPlayer = p.useCard(card)
			c.Reply(strconv.Itoa(p.seenPlayer))
		case "SeePlayer":
			blue := c.String() == "blue"
			if blue {
				p.knowledge.MustBeBlue(p.seenPlayer)
			} else {
				p.knowledge.MustBeRed(p.seenPlayer)
			}
			c.Reply(blueRed(p.accuse(p.seenPlayer, blue)))
		case "SeePlayerClaim":
			from := c.Int()
			about := c.Int()
			blue := c.String() == "blue"
			var claimant, claim Knowledge
			claimant.MustBeRed(from).Or(claim.MustBe(about, blue))
			p.knowledge.And(&claimant)
			p.public.And(&claimant)
		case "ProposeMission":
			c.ReplyInts(p.proposeMission(p.rules.MissionPlayers[p.mission]))
		case "VoteOnMission":
			p.missionPlayers = c.Ints()
			c.Reply(yesNo(p.vote(p.missionPlayers)))
		case "VoteResult":
			// TODO
		case "UseVetoCard":
			if p.veto(p.missionPlayers, p.missionVote) {
				c.Reply("veto")
			} else {
				c.Reply("")
			}
		case "MissionVetoed":
			p.vetoCards--
			// TODO
		case "UseOpenCard":
			if player, use := p.useOpenCard(); use {
				c.Reply(strconv.Itoa(player))
			} else {
				c.Reply("")
			}
		case "GoOnMission":
			c.Reply(blueRed(p.playMission(false)))
		case "GoOnMissionOpen":
			c.Reply(blueRed(p.playMission(true)))
		case "OpenMissionResult":
			player := c.Int()
			blue := c.String() == "blue"
			p.open = append(p.open, openMission{player, blue})
			if !blue {
				p.knowledge.MustBeRed(player)
				p.public.MustBeRed(player)
			}
		case "UseSeeMissionCard":
			if player, use := p.useSeeMissionCard(); use {
				c.Reply(strconv.Itoa(player))
			} else {
				c.Reply("")
			}
		case "SeeMission":
			blue := c.String() == "blue"
			if blue {
				p.knowledge.MustBeBlue(p.seenPlayer)
			} else {
				p.knowledge.MustBeRed(p.seenPlayer)
			}
			c.Reply(blueRed(p.accuse(p.seenPlayer, blue)))
		case "SeeMissionClaim":
			from := c.Int()
			about := c.Int()
			blue := c.String() == "blue"
			p.claims = append(p.claims, missionClaim{from, about, blue})
		case "MissionResult":
			fails := c.Int()
			blue := c.String() == "blue"
			// remove open plays
			for _, o := range p.open {
				if !o.blue {
					fails--
				}
				for i, pl := range p.missionPlayers {
					if pl == o.player {
						p.missionPlayers = append(p.missionPlayers[:i], p.missionPlayers[i+1:]...)
						break
					}
				}
			repeat:
				for i, claim := range p.claims {
					if claim.about == o.player {
						if claim.blue != o.blue {
							p.knowledge.MustBeRed(claim.from)
							p.public.MustBeRed(claim.from)
						}
						p.claims = append(p.claims[:i], p.claims[i+1:]...)
						goto repeat
					}
				}
			}
			// update knowledge
			if fails > 0 {
				p.knowledge.RemoveMissionResult(p.missionPlayers, fails)
				p.public.RemoveMissionResult(p.missionPlayers, fails)
			}
			// check claims
			for _, c := range p.claims {
				if !c.blue {
					if fails == 0 {
						p.knowledge.MustBeRed(c.from)
						p.public.MustBeRed(c.from)
					} else {
						var from, about Knowledge
						from.MustBeRed(c.from).Or(about.MustBeRed(c.about))
						p.knowledge.And(&from)
						p.public.And(&from)
					}
				}
			}
			p.claims = p.claims[:0]
			if blue {
				p.blueVictories++
			} else {
				p.redVictories++
			}
		case "GameOver", "GameAborted":
			p.vetoCards = 0
			p.blueVictories = 0
			p.redVictories = 0
			p.knowledge = Knowledge{}
			p.public = Knowledge{}
		case "Error":
			fmt.Fprintln(os.Stderr, c.String())
		}
	}
}

func (p *AIPlayer) setupKnowledge(hints []int) {
	reds := 0
	for _, c := range p.rules.Characters {
		if !c.Blue() {
			reds++
		}
	}
	p.knowledge.Init(len(p.names), reds)
	p.public.Init(len(p.names), reds)
	if p.character.Blue() {
		p.knowledge.MustBeBlue(p.index)
	} else {
		p.knowledge.MustBeRed(p.index)
		for _, hint := range hints {
			p.knowledge.MustBeRed(hint)
		}
	}
	switch p.character {
	case Merlin:
		for _, hint := range hints {
			p.knowledge.MustBeRed(hint)
		}
	case Parcival:
		var merlin0, merlin1 Knowledge
		p.knowledge.And(merlin0.MustBeBlue(hints[0]).Xor(merlin1.MustBeBlue(hints[1])))
	case Couple1, Couple2:
		for _, hint := range hints {
			p.knowledge.MustBeBlue(hint)
		}
	}
}

func (p *AIPlayer) distributeCards(cards []Card) []int {
	list, _ := p.makeList(nil)
	players := make([]int, len(cards))
	for i := range players {
		players[i] = list[i+1].index
	}
	return players
}

func (p *AIPlayer) useCard(card Card) int {
	if p.character.Blue() {
		list, _ := p.makeList(nil)
		return list[len(list)/2].index
	} else {
		list, _ := p.makeList(&p.public)
		return list[1].index
	}
}

func (p *AIPlayer) accuse(player int, blue bool) bool {
	if p.character.Blue() {
		return blue
	}
	if rand.Intn(3) == 0 {
		return !blue
	}
	return blue
}

func (p *AIPlayer) proposeMission(num int) []int {
	list, _ := p.makeList(nil)
	players := make([]int, num)
	for i := range players {
		players[i] = list[i].index
	}
	return players
}

func (p *AIPlayer) vote(players []int) bool {
	if p.attempt+p.vetoCards >= 4 {
		return true
	}
	if p.mission == 0 {
		return p.names[p.index] != "Raphael"
	}
	blue := p.knowledge.CalculateMissionChance(players, p.rules.RequiredFails[p.mission])
	if p.character.Blue() {
		return blue > 0
	} else {
		pblue := p.public.CalculateMissionChance(players, p.rules.RequiredFails[p.mission])
		if pblue == 0 {
			return false
		}
		if blue == 0 {
			return true
		}
		return rand.Intn(4) == 0
	}
}

func (p *AIPlayer) veto(players []int, vote []bool) bool {
	if p.character.Blue() {
		blue := p.knowledge.CalculateMissionChance(players, p.rules.RequiredFails[p.mission])
		if blue == 0 {
			return true
		}
	} else if p.attempt+p.vetoCards >= 4 {
		return true
	}
	return false
}

func (p *AIPlayer) useOpenCard() (int, bool) {
	if p.mission < 2 {
		return 0, false
	}
	if !p.character.Blue() {
		i := p.index
		for i == p.index {
			i = p.missionPlayers[rand.Intn(len(p.missionPlayers))]
		}
		return i, true
	}
	list, n := p.makeList(&p.knowledge)
	plist, pn := p.makeList(&p.public)
	best := p.missionPlayers[0]
	bestr := 0
	for _, pl := range p.missionPlayers {
		if pl == p.index {
			continue
		}
		if list[pl].redness > bestr {
			bestr = list[pl].redness
			best = pl
		}
		if list[pl].redness == n && plist[pl].redness < pn {
			return pl, true
		}
	}
	return best, true
}

func (p *AIPlayer) useSeeMissionCard() (int, bool) {
	if p.mission < 1 {
		return 0, false
	}
	if !p.character.Blue() {
		i := p.index
		for i == p.index {
			i = p.missionPlayers[rand.Intn(len(p.missionPlayers))]
		}
		return i, true
	}
	list, n := p.makeList(&p.knowledge)
	plist, pn := p.makeList(&p.public)
	best := p.missionPlayers[0]
	bestr := 0
	for _, pl := range p.missionPlayers {
		if pl == p.index {
			continue
		}
		if list[pl].redness > bestr && list[pl].redness < n {
			bestr = list[pl].redness
			best = pl
		}
		if list[pl].redness == n && plist[pl].redness < pn {
			return pl, true
		}
	}
	return best, true
}

func (p *AIPlayer) playMission(open bool) bool {
	if p.character.Blue() {
		return true
	}
	fails := p.rules.RequiredFails[p.mission]
	reds := 0
	for _, pl := range p.missionPlayers {
		if p.knowledge.Red(pl) {
			reds++
		}
	}
	if p.redVictories == 2 && p.knowledge.CalculateMissionChance(p.missionPlayers, fails) == 0 {
		return false
	}
	if p.blueVictories == 2 && (!open || reds <= fails) {
		return false
	}
	if open {
		return true
	}
	return rand.Intn(reds) >= fails
}

type playerEntry struct {
	index   int
	redness int
}

func (p *AIPlayer) makeList(k *Knowledge) ([]playerEntry, int) {
	if k == nil {
		if p.character.Blue() {
			k = &p.knowledge
		} else {
			k = &p.public
		}
	}
	list := make([]playerEntry, len(p.names))
	redness := make([]int, len(p.names))
	k.CalculateRedness(redness)
	n := p.public.Combinations()
	for i := range list {
		list[i].index = i
		list[i].redness = redness[i]
	}
	list[p.index].redness = -1
	sort.Slice(list, func(i, j int) bool { return list[i].redness < list[j].redness })
	return list, n
}
