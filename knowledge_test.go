package avalon

import (
	"fmt"
	"testing"
)

func TestKnowledge(t *testing.T) {
	var k Knowledge
	k.Init(10, 4)
	k.MustBeBlue(5)
	k.RemoveMissionResult([]int{0, 1, 2}, 1)
	k.RemoveMissionResult([]int{0, 3, 4}, 2)
	red := make([]int, 10)
	k.CalculateRedness(red)
	fmt.Println(red, k.Combinations())
	fmt.Println(k.CalculateMissionChance([]int{8, 9}, 2))
	//t.Fail()
}

func BenchmarkRedness(t *testing.B) {
	var k Knowledge
	k.Init(10, 4)
	k.MustBeBlue(5)
	k.RemoveMissionResult([]int{0, 1, 2}, 1)
	k.RemoveMissionResult([]int{0, 3, 4}, 2)
	red := make([]int, 10)
	t.ResetTimer()
	for i := 0; i < t.N; i++ {
		k.CalculateRedness(red)
	}
}

func BenchmarkIterate(t *testing.B) {
	var k Knowledge
	k.Init(10, 4)
	k.MustBeBlue(5)
	k.RemoveMissionResult([]int{0, 1, 2}, 1)
	k.RemoveMissionResult([]int{0, 3, 4}, 2)
	x := 0
	t.ResetTimer()
	for i := 0; i < t.N; i++ {
		for j := 0; j < 1<<10; j++ {
			if k.Possible(uint16(j)) {
				x++
			}
		}
	}
}

func BenchmarkCombinations(t *testing.B) {
	var k Knowledge
	k.Init(10, 4)
	k.MustBeBlue(5)
	k.RemoveMissionResult([]int{0, 1, 2}, 1)
	k.RemoveMissionResult([]int{0, 3, 4}, 2)
	x := 0
	t.ResetTimer()
	for i := 0; i < t.N; i++ {
		x += k.Combinations()
	}
}

func BenchmarkChance1(t *testing.B) {
	var k Knowledge
	k.Init(10, 4)
	k.MustBeBlue(5)
	k.RemoveMissionResult([]int{0, 1, 2}, 1)
	k.RemoveMissionResult([]int{0, 3, 4}, 2)
	players := []int{1, 7, 8, 9}
	x := 0
	t.ResetTimer()
	for i := 0; i < t.N; i++ {
		x += k.CalculateMissionChance(players, 1)
	}
}

func BenchmarkChance2(t *testing.B) {
	var k Knowledge
	k.Init(10, 4)
	k.MustBeBlue(5)
	k.RemoveMissionResult([]int{0, 1, 2}, 1)
	k.RemoveMissionResult([]int{0, 3, 4}, 2)
	players := []int{1, 7, 8, 9}
	x := 0
	t.ResetTimer()
	for i := 0; i < t.N; i++ {
		x += k.CalculateMissionChance(players, 2)
	}
}
