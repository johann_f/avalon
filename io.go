package avalon

import (
	"bufio"
	"io"
	"strconv"
	"strings"
)

type Client struct {
	in   *bufio.Scanner
	out  io.Writer
	line string
	buf  []byte
}

func NewClient(r io.Reader, w io.Writer) *Client {
	return &Client{in: bufio.NewScanner(r), out: w}
}

func (c *Client) Next() (string, bool) {
	if !c.in.Scan() {
		return "", false
	}
	c.line = c.in.Text()
	i := strings.IndexByte(c.line, ':')
	if i == -1 {
		i = len(c.line)
	}
	r := strings.TrimSpace(c.line[:i])
	if i+1 >= len(c.line) {
		c.line = ""
	} else {
		c.line = c.line[i+1:]
	}
	return r, true
}

func (c *Client) String() string {
	i := strings.IndexByte(c.line, ',')
	if i == -1 {
		i = len(c.line)
	}
	r := strings.TrimSpace(c.line[:i])
	if i+1 >= len(c.line) {
		c.line = ""
	} else {
		c.line = (c.line)[i+1:]
	}
	return r
}

func (c *Client) Strings() []string {
	sp := strings.Split(c.line, ",")
	for i := range sp {
		sp[i] = strings.TrimSpace(sp[i])
	}
	c.line = ""
	return sp
}

func (c *Client) Int() int {
	i, _ := c.int()
	return i
}

func (c *Client) int() (int, bool) {
	if c.line == "" {
		return 0, false
	}
	i := strings.IndexByte(c.line, ',')
	if i == -1 {
		i = len(c.line)
	}
	s := strings.TrimSpace(c.line[:i])
	i64, err := strconv.ParseInt(s, 0, 32)
	if err != nil {
		return 0, false
	}
	if i+1 >= len(c.line) {
		c.line = ""
	} else {
		c.line = c.line[i+1:]
	}
	return int(i64), true
}

func (c *Client) Character() Character {
	r, _ := c.character()
	return r
}

func (c *Client) character() (Character, bool) {
	if c.line == "" {
		return 0, false
	}
	i := strings.IndexByte(c.line, ',')
	if i == -1 {
		i = len(c.line)
	}
	s := strings.TrimSpace(c.line[:i])
	var r Character
	if !r.Parse(s) {
		return 0, false
	}
	if i+1 >= len(c.line) {
		c.line = ""
	} else {
		c.line = c.line[i+1:]
	}
	return r, true
}

func (c *Client) Card() Card {
	r, _ := c.card()
	return r
}

func (c *Client) card() (Card, bool) {
	if c.line == "" {
		return 0, false
	}
	i := strings.IndexByte(c.line, ',')
	if i == -1 {
		i = len(c.line)
	}
	s := strings.TrimSpace(c.line[:i])
	var r Card
	if !r.Parse(s) {
		return 0, false
	}
	if i+1 >= len(c.line) {
		c.line = ""
	} else {
		c.line = c.line[i+1:]
	}
	return r, true
}

func (c *Client) Ints() []int {
	var out []int
	for {
		i, ok := c.int()
		if !ok {
			return out
		}
		out = append(out, i)
	}
}

func (c *Client) Characters() []Character {
	var out []Character
	for {
		i, ok := c.character()
		if !ok {
			return out
		}
		out = append(out, i)
	}
}

func (c *Client) Cards() []Card {
	var out []Card
	for {
		i, ok := c.card()
		if !ok {
			return out
		}
		out = append(out, i)
	}
}

func (c *Client) Votes() []bool {
	var out []bool
	for {
		s := c.String()
		switch s {
		case "yes":
			out = append(out, true)
		case "no":
			out = append(out, false)
		default:
			return out
		}
	}
}

func (c *Client) Reply(s string) {
	c.buf = append(c.buf[:0], s...)
	c.buf = append(c.buf, '\n')
	c.out.Write(c.buf)
}

func (c *Client) ReplyInt(i int) {
	c.buf = strconv.AppendInt(c.buf[:0], int64(i), 10)
	c.buf = append(c.buf, '\n')
	c.out.Write(c.buf)
}

func (c *Client) ReplyInts(i []int) {
	if len(i) == 0 {
		c.out.Write([]byte{'\n'})
		return
	}
	c.buf = c.buf[:0]
	for _, i := range i {
		c.buf = strconv.AppendInt(c.buf, int64(i), 10)
		c.buf = append(c.buf, ',', ' ')
	}
	c.buf[len(c.buf)-2] = '\n'
	c.out.Write(c.buf[:len(c.buf)-1])
}

type Connection struct {
	in  *bufio.Scanner
	out io.WriteCloser
	buf []byte
	err string
}

func NewConnection(r io.Reader, w io.WriteCloser) *Connection {
	return &Connection{in: bufio.NewScanner(r), out: w}
}

func (c *Connection) Write(msg string, args ...interface{}) {
	c.buf = append(c.buf[:0], msg...)
	c.buf = append(c.buf, ':', ' ')
	for _, a := range args {
		switch a := a.(type) {
		case int:
			c.buf = strconv.AppendInt(c.buf, int64(a), 10)
			c.buf = append(c.buf, ',', ' ')
		case string:
			c.buf = append(c.buf, a...)
			c.buf = append(c.buf, ',', ' ')
		case Character:
			c.buf = append(c.buf, a.String()...)
			c.buf = append(c.buf, ',', ' ')
		case Card:
			c.buf = append(c.buf, a.String()...)
			c.buf = append(c.buf, ',', ' ')
		case []int:
			for _, a := range a {
				c.buf = strconv.AppendInt(c.buf, int64(a), 10)
				c.buf = append(c.buf, ',', ' ')
			}
		case []string:
			for _, a := range a {
				c.buf = append(c.buf, a...)
				c.buf = append(c.buf, ',', ' ')
			}
		case []Character:
			for _, a := range a {
				c.buf = append(c.buf, a.String()...)
				c.buf = append(c.buf, ',', ' ')
			}
		case []Card:
			for _, a := range a {
				c.buf = append(c.buf, a.String()...)
				c.buf = append(c.buf, ',', ' ')
			}
		case []bool:
			for _, a := range a {
				if a {
					c.buf = append(c.buf, "yes, "...)
				} else {
					c.buf = append(c.buf, "no, "...)
				}
			}
		}
	}
	c.buf[len(c.buf)-2] = '\n'
	c.out.Write(c.buf[:len(c.buf)-1])
}

func (c *Connection) ReadInt() int {
	if !c.in.Scan() {
		c.err = "reply expected"
		return 0
	}
	i64, err := strconv.ParseInt(strings.TrimSpace(c.in.Text()), 0, 32)
	if err != nil {
		c.err = "reply must be an integer"
	}
	return int(i64)
}

func (c *Connection) ReadOptionalInt() (int, bool) {
	if !c.in.Scan() {
		c.err = "reply expected"
		return 0, false
	}
	r := strings.TrimSpace(c.in.Text())
	if r == "" {
		return 0, false
	}
	i64, err := strconv.ParseInt(r, 0, 32)
	if err != nil {
		c.err = "reply must be an integer or empty"
	}
	return int(i64), true
}

func (c *Connection) ReadInts() []int {
	if !c.in.Scan() {
		c.err = "reply expected"
		return nil
	}
	sp := strings.Split(c.in.Text(), ",")
	ints := make([]int, len(sp))
	for i, s := range sp {
		x, err := strconv.ParseInt(strings.TrimSpace(s), 0, 32)
		if err != nil {
			c.err = "reply must be a list of integers"
		}
		ints[i] = int(x)
	}
	return ints
}

func (c *Connection) ReadBool(t, f string) bool {
	if !c.in.Scan() {
		c.err = "reply expected"
		return false
	}
	r := strings.TrimSpace(c.in.Text())
	if r == t {
		return true
	}
	if r != f {
		if f == "" {
			c.err = "reply must be " + t + " or empty"
		} else {
			c.err = "reply must be " + t + " or " + f
		}
	}
	return false
}

func (c *Connection) Close() error {
	return c.out.Close()
}

func Pipe() (*Client, *Connection) {
	r1, w1 := io.Pipe()
	r2, w2 := io.Pipe()
	return NewClient(r1, w2), NewConnection(r2, w1)
}
