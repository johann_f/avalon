package main

import (
	"fmt"
	"io"
	"math/rand"
	"os"
	"time"

	"bitbucket.org/johann_f/avalon"
)

type rw struct {
	io.Reader
	io.Writer
}

func runInteractiveGame(name string, rules *avalon.Rules) {
	names := []string{"Martin", "Anton", "Raphael", "Stefanie", "Johann", "Isa", "Max", "Schirin", "Lukas", "Johannes", "Valentin", "Axel"}
	for i := range names {
		j := i + rand.Intn(len(names)-i)
		names[i], names[j] = names[j], names[i]
	}
	for i, n := range names {
		if n == name {
			names[i], names[0] = names[0], names[i]
			goto next
		}
	}
	names[0] = name
	names = names[:len(rules.Characters)]
next:
	players := make([]*avalon.Connection, len(rules.Characters))
	for i := range players {
		if i == 0 {
			players[i] = avalon.NewConnection(os.Stdin, os.Stdout)
		} else {
			cl, c := avalon.Pipe()
			players[i] = c
			go (&avalon.AIPlayer{}).Run(cl)
		}
	}
	avalon.RunGame(rules, names, players)
}

func runGames(rules *avalon.Rules, n int) int {
	players := make([]*avalon.Connection, len(rules.Characters))
	for i := range players {
		cl, c := avalon.Pipe()
		players[i] = c
		go (&avalon.AIPlayer{}).Run(cl)
	}
	blue := 0
	for i := 0; i < n; i++ {
		r := avalon.RunGame(rules, nil, players)
		switch r {
		case avalon.BlueVictory:
			blue++
		case avalon.RedVictory:
		case avalon.Error:
			return -n
		}
	}
	return blue
}

func main() {
	rules := &avalon.Rules{
		Characters:     []avalon.Character{avalon.Merlin, avalon.Parcival, avalon.Couple1, avalon.Couple1, avalon.Couple2, avalon.Couple2, avalon.Mordred, avalon.Morgana, avalon.Assassin, avalon.Oberon},
		MissionPlayers: []int{3, 4, 4, 5, 5},
		RequiredFails:  []int{1, 1, 1, 2, 1},
		Cards: avalon.DrawCardsFrom([]avalon.Card{
			avalon.SeePlayer, avalon.SeePlayer, avalon.SeePlayer,
			avalon.Veto, avalon.Veto, avalon.Veto,
			avalon.SeeMission, avalon.SeeMission,
			avalon.OpenMission, avalon.OpenMission,
		}, 2),
	}
	const interactive = false

	rand.Seed(time.Now().Unix())
	if interactive {
		runInteractiveGame("Player", rules)
	} else {
		const n = 10000
		fmt.Printf("Blue wins %.0f%%\n", float64(runGames(rules, n))/n*100)
	}
}
